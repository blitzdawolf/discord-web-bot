var express = require("express");
var bodyParser = require("body-parser");
var api = require(__dirname + '/app');
var app = express();
const path = require('path');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname+'/app'))

app.get('/',function(req,res){
    res.sendfile("index.html");
});

app.get('/logs/:name', function(req,res){
    let name = req.params.name;
    


    console.log(req);
});

app.get('/edit/script/:bot/:scriptName', (request, response) => {
    console.log(request.params);
    response.sendFile(path.join(__dirname, '/app/edit.html'));
})
app.post('/edit/script/:bot/:scriptName', (request, response) => {
    api.editScript(request,response);
    response.redirect('/');
    // response.json( { "test":"test" } );
});

app.get('/getbots',     t);

app.post('/activate',   t);
app.post('/disconect',  t);
app.post('/addbot',     t)
app.post('/restart',    t);

function t(request, response){
    let l = api.handleRequest(request,response);
    response.json(l);
}

app.listen(3000,function(){
    console.log("Started on PORT 3000");
})