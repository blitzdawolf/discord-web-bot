var lastData = {};

function loadDoc() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // document.getElementById("demo").innerHTML = this.responseText;
            let dat = JSON.parse(this.responseText);            

            if(Object.keys(lastData).length != Object.keys(dat).length){
                document.getElementById('i').innerHTML='';
                Object.keys(dat).forEach((i)=>{

                    let t = document.createElement('DIV');

                    let nameTag = document.createElement('H2');
                    nameTag.innerText = dat[i].name;
                    let button = document.createElement('BUTTON');
                    button.innerText = "activate";
                    button.onclick = () => {activateBot(dat[i].name)};
                    
                    let Log = document.createElement('BUTTON');
                    Log.innerText = "Details";

                    let details = document.createElement('BUTTON');
                    details.innerText = "details";
                    
                    let Deactivate = document.createElement('BUTTON');
                    Deactivate.innerText = "Deactivate";
                    Deactivate.onclick = () => {deactivateBot(dat[i].name)};

                    button.classList = 'btn btn-success ' + dat[i].name + '-activate';
                    Deactivate.classList = 'btn btn-danger ' + dat[i].name + '-deactivate';

                    Log.setAttribute('data-toggle','modal');
                    Log.setAttribute('data-target','#details');

                    Log.onclick = () => { cName = dat[i].name};

                    if(!dat[i].alive)
                        Deactivate.setAttribute('disabled', false);
                    else
                        button.setAttribute('disabled',true);
                        
                    Log.classList = 'btn btn-secondary';
                    details.classList = 'btn btn-secondary';


                    let group = document.createElement('DIV');

                    group.classList = 'btn-group';
                    group.role = 'group';

                    t.classList = 'bot-list-item';
                    t.append(nameTag);
                    group.append(button);
                    group.append(Log);
                    // group.append(details);
                    group.append(Deactivate);
                    t.append(group);

                    document.getElementById('i').append(t);
                });
                lastData = dat;
            }
            else
            {
                //for(let i = 0; i < dat.length;i++){
                Object.keys(dat).forEach((i)=>{
                    if(dat[i].alive != lastData[i].alive){
                        let a = document.querySelector('.' + dat[i].name + '-activate');
                        let d = document.querySelector('.' + dat[i].name + '-deactivate');

                        console.log(dat[i]);

                        if(!dat[i].alive){
                            console.log('bot is not alive in this session');
                            a.removeAttribute('disabled');
                            d.setAttribute('disabled',true);
                        }
                        else{
                            a.setAttribute('disabled',true);
                            d.removeAttribute('disabled');
                        }
                        //there is a diference
                    }
                    // Check for any diference
                });
                lastData = dat;
            }
        }
    };
    xhttp.open("GET", "/getbots", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("");
}

var cName;
setInterval(() => {
    setDetailModal();
}, 750);

function setDetailModal(){
    if(!cName){
        return;
    }
    //cName = data;*/
    let currentData = lastData[cName];

    let nameTag = document.getElementById('detail-bot-name');
    nameTag.value = currentData.name;
    let prefixTag = document.getElementById('detail-bot-prefix');
    prefixTag.value = currentData.prefix;

    let a = document.getElementById('detail-bot-activate');
    let d = document.getElementById('detail-bot-deactivate');
    let r = document.getElementById('detail-bot-restart');

    r.onclick = () => {restartBot(cName)};

    if(!currentData.alive){
        a.removeAttribute('disabled');
        d.setAttribute('disabled',true);
        a.onclick=()=>{activateBot(currentData.name)};
    }
    else{
        a.setAttribute('disabled',true);
        d.removeAttribute('disabled');
        d.onclick = () => {deactivateBot(currentData.name)};
    }
}

function restartBot(name){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            
        }
    };
    xhttp.open("POST", "/restart", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("name="+name);
}

function activateBot(name){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            
        }
    };
    xhttp.open("POST", "/activate", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("name="+name);
}

function deactivateBot(name){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
        }
    };
    xhttp.open("POST", "/disconect", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("name="+name);
}

function addBot(){
    let data = {
        "name": document.getElementById('name').value,
        "token": document.getElementById('token').value,
        "game": document.getElementById('game').value,
        "prefix":document.getElementById('prefix').value
    }

    let uri = "";
    Object.keys(data).forEach((o)=>{
        uri+=o + "=" + data[o] + "&";
    });
    uri = uri.substring(0,uri.length-1);
    console.log(uri);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            let dat = JSON.parse(this.responseText);
            console.log(dat);
            if(dat.success){
                location.reload(); 
            }
        }
    };
    xhttp.open("POST", "/addbot", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(uri);

    loadDoc();
    // alert('Adding a new bot');
}

window.onload = function(){
    loadDoc();
    setInterval(loadDoc,500);

    document.getElementById('addbot-btn').onclick = addBot;
}