var express = require("express");
var bodyParser = require("body-parser");
var url = require('url');
var fs = require('fs');
var fork = require('child_process').fork;

var t = { }

module.exports = {
    handleRequest: function(request, response) {
        var b = JSON.parse(fs.readFileSync(__dirname+'/../data/json/test.json'));

        var path = url.parse(request.url).pathname;

        if(path=='/activate'){
            var body=request.body;
            if(!body.name){
                return{"response":"No name was given"};
                // response.end('No name was given');
            }
            if(b.bots[body.name]){
                if(t[body.name]){
                    return {"response": "this bot already has been started"};
                }else{
                    b.bots[body.name].alive=true;
                    fs.writeFile(__dirname+'/../data/json/test.json', JSON.stringify(b,null,2), (err) => {});
                    let child = fork(__dirname + '/../bot/index.js', ['--name='+body.name], {});
                    child.send({'start': true});
                    t[body.name] = child;

                    let e = {"response":""+body.name};
                    return e;
                }
            }
        }

        if(path == '/disconect'){
            var body=request.body;
            if(!body.name)
                return{"response":'No name was given'};
            if(b.bots[body.name] && t[body.name]){
                b.bots[body.name].alive=false;
                fs.writeFile(__dirname+'/../data/json/test.json', JSON.stringify(b,null,2), (err) => {});
                t[body.name].send({'stop':true});
                setTimeout(() => {
                    delete t[body.name];
                }, 1000);
                return{"response":'Removed ' + body.name};
            }
        }

        if(path == '/restart'){
            var body=request.body;
            if(!body.name)
                return{"response":'No name was given'};
            if(b.bots[body.name] && t[body.name]){
                b.bots[body.name].alive=false;
                fs.writeFile(__dirname+'/../data/json/test.json', JSON.stringify(b,null,2), (err) => {});
                t[body.name].send({'stop':true});
                setTimeout(() => {
                    delete t[body.name];
                    let c = request;
                    c.url = '/activate'
                    this.handleRequest(c,response);
                }, 1000);
                return{"response":'Removed ' + body.name};
            }
        }

        if(path === '/getbots'){
            let a = {};

            Object.keys(b.bots).forEach((t)=>{
                let toSend = {
                    "name":b.bots[t].name,
                    "game":b.bots[t].game,
                    "alive":b.bots[t].alive,
                    "prefix":b.bots[t].prefix
                }

                a[b.bots[t].name] = toSend;
                // a.push(toSend);
            })
            return a;
        }

        if(path==='/addbot'){
            var body=request.body;
            if(body.token && body.name && body.token.length > 50){
                if(!b.bots[body.name]){
                    let newBot = {
                        "token": body.token,
                        "name": body.name,
                        "alive": false
                    }
                    if(body.prefix.length > 0){
                        newBot.prefix = body.prefix;
                    }
                    if(body.game.length > 0){
                        newBot.game = body.game;
                    }
                    b.bots[body.name] = newBot;
                    fs.writeFile(__dirname+'/../data/json/test.json', JSON.stringify(b, null, 2), (err) => {});
                    return {"success":true}
                }
            }
            return {"error":"there was a error"};
        }

        if(path==='/edit/script'){
            var body=request.body;
            console.log(body);
            if(body.botName && body.scriptName && body.discription && body.runCommand){
                fs.readFile(__dirname + '/../bot/template.tmpl','utf8', (err,data)=>{
                    data = data
                        .replace('@className',      body.scriptName)
                        .replace('@name',           body.scriptName)
                        .replace('@discription',    body.discription)
                        .replace('@runCommand',     body.runCommand)
                        .replace('@bot',            body.botName);
                    console.log(data);
                });
            }
            return {'response': 'test'};
        }
    },
    editScript(request, response){
        let parms = request.params;
        var body=request.body;
        console.log(body);
        if(parms.bot && parms.scriptName && body.discription && body.runCommand){
            fs.readFile(__dirname + '/../bot/template.tmpl','utf8', (err,data)=>{
                data = data
                    .replace('@className',      parms.scriptName)
                    .replace('@name',           parms.scriptName)
                    .replace('@discription',    body.discription)
                    .replace('@runCommand',     body.runCommand)
                    .replace('@bot',            parms.bot);
                console.log(data);
            });
        }
        // return {'response': 'test'};
    }
};