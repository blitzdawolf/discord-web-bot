const args = require('minimist')(process.argv.slice(2))
let botname = args['name']; //flavio
let botinfo = require(__dirname + '/../data/json/test.json').bots[botname];
const bot = require(__dirname + '/bot');

var runBot = undefined;

console.log(botinfo);

process.on('message', (msg) => {
    if(msg.start){
        runBot = new bot(botinfo);
    }
    if(msg.stop){
        console.log('test');
        runBot.stop();
    }
    if(msg.restart){
        runBot.stop();
        runBot = new bot(botinfo);
    }
});