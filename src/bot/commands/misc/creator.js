const {Command} = require('discord.js-commando');
const { RichEmbed} = require('discord.js');

module.exports = class creator extends Command{
    constructor(client){
        super(client,{
            name:"creator",
            aliases: ["created"],
            group:"misc",
            memberName:"creator",
            description:"Show who made this bot"
        });
    }
    run(msg,args){
        let embed = new RichEmbed()
        .setTitle('Creator')
        .setDescription('this bot system has been made by BlitzDaWolf');
        msg.embed(embed);
    }
}