// Imports for making the bot work
const { CommandoClient } = require('discord.js-commando');
const fs = require('fs');
const path = require('path');

class bot{

    // class constructor
    constructor(data){
        let parms = {};
        parms.disableEveryone = false;
        parms.owner = '254307593978249217';
        if(data.prefix)
            parms.commandPrefix = data.prefix;
        else
            parms.commandPrefix= '!$-';

        let c = new CommandoClient(parms);
        
        this.setClient(c);
        this.setData(data);

        c.registry
            .registerDefaultTypes()
            .registerGroups([
                ['misc','Misc'],
                ['moderation', 'Moderation']
            ])
            .registerDefaultGroups()
            .registerDefaultCommands({
                
            })
            .registerCommandsIn(path.join(__dirname, 'commands'));

        c.on('ready', 
            (test) => this.logedin(this));
        c.on('message', 
            (msg) => this.msgResive(msg, this));
        c.login(data.token);

        c.on('error', console.error);
    }

    // Sets the data
    setData(data){
        this._data = data;
    }

    // Get the data
    getData(){
        return  this._data;
    }

    // Get the current discord cliemt
    getClient(){
        return this._client;
    }

    //Sets the discord clinet
    setClient(c){
        this._client = c;
    }

    deleteClient(){
        delete this._client;
    }

    // Stop the bot
    stop() {
        this._client.destroy();
        this.deleteClient();
    }

    msgResive(msg, data){
        if(msg.channel.guild){
            let datas = {
                "msg": msg.content,
                "author": {
                    "username": msg.author.username,
                    "id": msg.author.id
                },
                "posted": msg.createdAt
            }
            fs.exists(__dirname + '/../data/bot_logs/' + data.getData().name + '.json', (e) => {
                if(e){
                    let previos = JSON.parse(fs.readFileSync(__dirname + '/../data/bot_logs/' + data.getData().name + '.json'));

                    if(!previos[msg.channel.guild.name]){
                        previos[msg.channel.guild.name] = {}
                    }
                    if(!previos[msg.channel.guild.name][msg.channel.name]){
                        previos[msg.channel.guild.name][msg.channel.name] = [];
                    }

                    previos[msg.channel.guild.name][msg.channel.name].push(datas);

                    fs.writeFile(__dirname + '/../data/bot_logs/' + data.getData().name + '.json', JSON.stringify(previos, null, 2),(s)=>{});
                }else{
                    let previos={};
                    if(!previos[msg.channel.guild.name]){
                        previos[msg.channel.guild.name] = {}
                    }
                    if(!previos[msg.channel.guild.name][msg.channel.name]){
                        previos[msg.channel.guild.name][msg.channel.name] = [];
                    }

                    previos[msg.channel.guild.name][msg.channel.name].push(datas);
                    fs.writeFile(__dirname + '/../data/bot_logs/' + data.getData().name + '.json',JSON.stringify(previos, null, 2),(s)=>{})
                }
            });
        }
        console.log(msg.content);
    }

    logedin(c) {
        console.log(c.getData().name + ' Logged in!');

        let amt = c.getClient().guilds.size;

        c.getData().servers = amt;
        console.log(c.getData().servers);

        if(c.getData().game)
            c.getClient().user.setActivity(c.getData().game);
        else
            c.getClient().user.setActivity('Default');
    }
}

module.exports = bot;